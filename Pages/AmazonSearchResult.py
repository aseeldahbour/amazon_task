from selenium.webdriver.common.by import By


class AmazonSearchResult:
    @staticmethod
    def get_url(driver):
        return driver.find_elements(By.XPATH, '//h2[@class="a-size-mini a-spacing-none a-color-base s-line-clamp-2"]/a')



