from selenium.webdriver.common.by import By


class AmazonSearchPage:
    @staticmethod
    def open_amazon_page(driver):
        driver.get("https://www.amazon.com/")

    @staticmethod
    def get_search_box(driver):
        return driver.find_element(By.NAME, "field-keywords")

    @staticmethod
    def search(driver, search_text):
        search_box = AmazonSearchPage.get_search_box(driver)
        search_box.send_keys(search_text)
        search_box.submit()

    @staticmethod
    def get_drop_down_box(driver):
        return driver.find_elements(By.XPATH, '//*[@id="searchDropdownBox"]')
