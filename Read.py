import csv


class Read:
    @staticmethod
    def read_file(filename):
        rows = []
        csv_data = open(str(filename), "r", encoding="utf8")
        content = csv.reader(csv_data)
        next(content, None)
        for row in content:
            rows.append(row)
        return rows
