import unittest
from ddt import ddt, data, unpack
import time

from selenium import webdriver

from Bing_pages.BingSearchPage import BingSearchPage
from Bing_pages.BingSearchResult import BingSearchResult
from Pages.AmazonSearchPage import AmazonSearchPage
from Pages.AmazonSearchResult import AmazonSearchResult
from Read import Read
from Write import Write


@ddt
class MainFunctions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.amazon_result = []
        cls.bing_result = []
        cls.amazon = []
        cls.bing = []
        cls.headers = ["search_text", "result_type"]
        cls.driver = webdriver.Chrome('../Driver/chromedriver.exe')
        time.sleep(10)
        print("setup class")

    @data(*Read.read_file("../Config/Amazon_search_text.csv"))
    @unpack
    def test_amazon_search(self, search_text, result_type):
        result_dict = {}
        result_dict.update({"search_text": search_text, "result_type": result_type})
        amazon_page = AmazonSearchPage()
        amazon_page.open_amazon_page(self.driver)
        amazon_page.get_drop_down_box(self.driver)
        amazon_page.search(self.driver, search_text + " " + result_type)
        elements = AmazonSearchResult()
        urls = elements.get_url(self.driver)
        if len(urls) > 0:
            i = 1
            for url in urls:
                url_text = url.get_attribute("href")
                if url_text:
                    title_text = url.text
                    result_dict.update({"url result found" + str(i): url_text, "title result found " + str(i): title_text})
                    if "url result found" + str(i) not in self.headers:
                        self.headers.append("url result found" + str(i))
                    if "title result found" + str(i) not in self.headers:
                        self.headers.append("title result found" + str(i))

                    i += 1
            if i == 1:
                result_dict.update({"url1": "url result not Found", "title1": "title result Not Found"})

        else:
            result_dict.update({"url1": "url result not Found", "title1": "title result Not Found"})
        self.amazon_result.append(result_dict)

    @data(*Read.read_file("../Config/Amazon_search_text.csv"))
    @unpack
    def not_test_bing_search(self, search_text, result_type):
        bing_dict = {}
        bing_dict.update({"search_text": search_text, "result_type": result_type})
        bing_page = BingSearchPage()
        bing_page.open_bing_page(self.driver)
        bing_page.search(self.driver, search_text + " " + result_type + " " + "amazon.com")
        elements = BingSearchResult()
        links = elements.get_links(self.driver)
        if len(links) > 0:
            i = 1
            for link in links:
                url_text = link.get_attribute("href")
                bing_dict.update({"link result found" + str(i): url_text})
                if "link result found" + str(i) not in self.headers:
                    self.headers.append("link result found" + str(i))
                i += 1
            if i == 1:
                bing_dict.update({"url1": "url result not Found"})

        else:
            bing_dict.update({"url1": "url result not Found"})
        self.bing_result.append(bing_dict)

    def not_test_check_results(self):
        for key in self.amazon_result.keys():
            if key == "url result found":
                self.amazon = self.amazon_result.values()

        for k in self.bing_result.keys():
            if k == "link result found":
                self.bing = self.bing_result.values()

        self.assertEqual(self.amazon, self.bing)

    @classmethod
    def tearDownClass(cls):
        Write.write_file("../Config/output.csv", cls.amazon_result, cls.headers)
        cls.driver.close()
        print("tear down")
